<?php

require '../../vendor/autoload.php';

session_start();

$ds = DIRECTORY_SEPARATOR;
$settingsPath = dirname(__DIR__) . $ds . 'settings' . $ds . 'index.php';

$reactWader = new \YaTDL\YaTDL(include($settingsPath));
$reactWader->start();
