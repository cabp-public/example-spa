<?php

namespace YaTDL\Models;

class Item
{

    private $oid;
    private $member;
    private $title;
    private $description;
    private $item;

    private $storedData;
    private $storedDataPath;

    public function __construct($oid = null)
    {
        // Fake data!
        $ds = DIRECTORY_SEPARATOR;
        $path = dirname(__DIR__) . $ds . '_data' . $ds . 'data.json';

        $this->storedData = json_decode(file_get_contents($path), true);
        $this->storedDataPath = $path;
    }

    private function getField($field)
    {
        $keys = array_keys($this->storedData);
        $keyFound = array_search($field, $keys);
        $output = $keyFound !== false ? $this->storedData[$field] : false;

        return $output;
    }

    public function get($field = false) {
        return $field ? $this->getField($field) : $this->item;
    }

    public function set($item) {
        try {
            $this->oid = count($this->storedData['items']) + 1;
            $this->member = $item['member'];
            $this->title = $item['title'];
            $this->description = $item['description'];

            $this->item = [
                'oid' => $this->oid,
                'member' => $this->member,
                'title' => $this->title,
                'description' => $this->description,
            ];

            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

    public function save() {
        sleep(2);

        try {
            $this->storedData['items'][] = $this->item;
            file_put_contents($this->storedDataPath, json_encode($this->storedData));

            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

    public static function delete($id) {
        sleep(1);

        // Fake data!
        $ds = DIRECTORY_SEPARATOR;
        $path = dirname(__DIR__) . $ds . '_data' . $ds . 'data.json';
        $storedData = json_decode(file_get_contents($path), true);

        try {
            $newData = [];
            $data = $storedData['items'];

            foreach ( $data as $item ) {
                if ( $id !== $item['$oid'] ) {
                    $newData[] = $item;
                }
            }

            $storedData['items'] = $newData;
            file_put_contents($path, json_encode($storedData));

            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

}