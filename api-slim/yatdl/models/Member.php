<?php

namespace YaTDL\Models;

class Member
{

    private $oid;
    private $name;
    private $email;
    private $profile;

    private $storedData;
    private $storedDataPath;

    public function __construct($oid = null)
    {
        // Fake data!
        $ds = DIRECTORY_SEPARATOR;
        $path = dirname(__DIR__) . $ds . '_data' . $ds . 'data.json';

        $this->storedData = json_decode(file_get_contents($path), true);
        $this->storedDataPath = $path;
    }

    private function getField($field)
    {
        $keys = array_keys($this->profile);
        $keyFound = array_search($field, $keys);
        $output = $keyFound !== false ? $this->profile[$field] : false;

        return $output;
    }

    public function get($field = false) {
        return $field ? $this->getField($field) : $this->profile;
    }

    public function set($profile) {
        try {
            $this->oid = count($this->storedData['members']) + 1;
            $this->name = $profile['name'];
            $this->email = $profile['email'];

            $this->profile = [
                'oid' => $this->oid,
                'name' => $this->name,
                'email' => $this->email,
            ];

            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

    public function save() {
        sleep(0.5);

        try {
            $this->storedData['members'][] = $this->profile;
            file_put_contents($this->storedDataPath, json_encode($this->storedData));

            return true;
        }
        catch (\Exception $e) {
            return false;
        }
    }

}