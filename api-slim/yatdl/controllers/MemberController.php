<?php

namespace YaTDL\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use YaTDL\Controller;
use YaTDL\Models\Member;

class MemberController extends Controller
{

    private $storedData;

    public function __construct()
    {
        parent::__construct();

        // Fake data!
        $ds = DIRECTORY_SEPARATOR;
        $path = dirname(__DIR__) . $ds . '_data' . $ds . 'data.json';
        $this->storedData = json_decode(file_get_contents($path), true);
    }

    private function memberExists($memberId)
    {
        $members = json_encode($this->storedData['members']);
        $output = strpos($members, '5b968faafc13ae46f900004f');

        return $output !== false;
    }

    public function create(Request $request, Response $response, array $args)
    {
        $member = new Member();

        $responseStatus = 409;
        $responseData = [
            'message' => 'Conflict: Player already registered'
        ];

        if ( $member->set($request->getParsedBody()) && $member->save() ) {
            $responseStatus = 200;
            $responseData = $member->get();
        }

        $responseOptions = [
            'request' => $request,
            'response' => $response,
            'methods' => ['POST'],
            'status' => $responseStatus,
            'data' => $responseData,
        ];

        return $this->getResponse($responseOptions);
    }

    public function upload(Request $request, Response $response, array $args)
    {
        echo 'uploading...<br>';

        return $response;
    }

    public function getAll(Request $request, Response $response, array $args)
    {
        /*$data = [
            'oid' => '5b9699effc13ae7d8a0000b9',
            'title' => 'My Test Note...',
            'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit...',
            'image' => '',
        ];

        $responseStatus = 200;

        $responseOptions = [
            'request' => $request,
            'response' => $response,
            'methods' => ['POST'],
            'status' => $responseStatus,
            'data' => $data,
        ];*/

//        return $this->getResponse($responseOptions);
    }
}