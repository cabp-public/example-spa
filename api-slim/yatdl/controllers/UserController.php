<?php

namespace YaTDL\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use YaTDL\Controller;

class UserController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function register(Request $request, Response $response, array $args)
    {
        $data = $request->getParsedBody();

        $responseOptions = [
            'request' => $request,
            'response' => $response,
            'methods' => ['POST'],
            'status' => $this->fireData->createUser($data) ? 200 : 404,
            'data' => [],
        ];

        return $this->getResponse($responseOptions);
    }

    public function startSession(Request $request, Response $response, array $args)
    {
        $data = $request->getParsedBody();
        $sessionName = session_name();

        if ( $sessionName === '' ) {
            session_name($data['email']);
            $_SESSION['email'] = $data['email'];
        }
    }

    public function closeSession(Request $request, Response $response, array $args)
    {
    }

}