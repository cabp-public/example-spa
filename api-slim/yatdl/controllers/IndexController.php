<?php

namespace YaTDL\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use YaTDL\Controller;

class IndexController extends Controller
{

    private $storedData;

    public function __construct()
    {
        parent::__construct();

        // Fake data!
        $ds = DIRECTORY_SEPARATOR;
        $path = dirname(__DIR__) . $ds . '_data' . $ds . 'data.json';
        $this->storedData = json_decode(file_get_contents($path), true);
    }

    public function index(Request $request, Response $response, array $args)
    {
        $members = json_encode($this->storedData['members']);

        echo '<pre>';
        var_dump(strpos($members, '5b968faafc13ae46f900004f'));
        echo '</pre>';

        /*$routeEntities = ['member', 'list', 'item' ];
        $output = [];

        // CRUD Operations
        foreach ( $routeEntities as $entity ) {
            $output[$entity] = [
                'endPoints' => [
                    '/create',
                    '[/read]',
                    '/update',
                    '/delete',
                ],
                'parameters' => []
            ];
        }

        $responseOptions = [
            'request' => $request,
            'response' => $response,
            'methods' => ['POST'],
            'status' => 200,
            'data' => $output,
        ];

        return $this->getResponse($responseOptions);*/
    }

    public function options(Request $request, Response $response, array $args)
    {
        $origins = $request->getHeader('Origin');
        $origin = isset($origins[0]) ? $origins[0] : '*';

        return $response
            ->withHeader('Access-Control-Allow-Origin', $origin)
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, OPTIONS')
            ->withHeader('Access-Control-Allow-Credentials', 'true')
            ->withHeader('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
    }

}