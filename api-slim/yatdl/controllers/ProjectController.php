<?php

namespace YaTDL\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
use YaTDL\Controller;
use YaTDL\DirectoryStructure;

class ProjectController extends Controller
{

    private $projectName;

    public function __construct()
    {
        parent::__construct();

        $this->projectName = $_SESSION['activeProject'];
    }

    public function index(Request $request, Response $response, array $args)
    {
        $data = $this->fireData->read();

        $responseOptions = [
            'request' => $request,
            'response' => $response,
            'methods' => ['GET'],
            'status' => 200,
            'data' => $data,
        ];

        return $this->getResponse($responseOptions);
    }

    public function getSingle(Request $request, Response $response, array $args)
    {
        $data = $this->fireData->read([$args['name']]);

        $responseOptions = [
            'request' => $request,
            'response' => $response,
            'methods' => ['GET'],
            'status' => 200,
            'data' => $data,
        ];

        return $this->getResponse($responseOptions);
    }

    public function getStructure(Request $request, Response $response, array $args)
    {
//        $projectName = $args['project'];
//        $dirStructure = new DirectoryStructure($projectName);
//        $data = $dirStructure->get();
        $data = [];

        $responseOptions = [
            'request' => $request,
            'response' => $response,
            'methods' => ['GET'],
            'status' => 200,
            'data' => $data,
        ];

        return $this->getResponse($responseOptions);
    }

    public function getEntryPoint(Request $request, Response $response, array $args)
    {
        $paths = $this->dirStructure->getAllPaths($args['name']);

        echo '<script language="JavaScript">';
        echo "function setData(button) {";
        echo "var el = document.getElementById('entryPoint');";
        echo "el.value = button.value;";
        echo "}";
        echo '</script>';

        echo '<form action="http://react-wader.local/projects/sample/entry/" method="post">';
        echo '<input type="text" id="entryPoint" name="entryPoint" style="width: 30em; text-align: left;"><br><br>';

        foreach ( $paths as $path ) {
            echo '<button type="button" onclick="setData(this)" value="' . $path . '" ';
            echo 'style="width: 30em; text-align: left;">' . $path . '</button><br>';
        }

        echo '<br><button type="submit" style="width: 30em;">SET</button>';

        echo '</form>';
    }

    public function setEntryPoint(Request $request, Response $response, array $args)
    {
        $data = $request->getParsedBody();
        $_SESSION['entryPoint'] = $data['entryPoint'];
    }

    public function setActive(Request $request, Response $response, array $args)
    {
        $path = $this->fireData->setActiveProject($args['name']);
        $status = 500;

        if ( $path !== false ) {
            $status = 200;
            $this->projectName = $args['name'];
        }

        $responseOptions = [
            'request' => $request,
            'response' => $response,
            'status' => $status
        ];

        return $this->getResponse($responseOptions);
    }
}
