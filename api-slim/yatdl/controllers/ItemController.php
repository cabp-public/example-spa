<?php

namespace YaTDL\Controllers;

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use YaTDL\Controller;
use YaTDL\Models\Item;

class ItemController extends Controller
{

    private $storedData;

    public function __construct()
    {
        parent::__construct();

        // Fake data!
        $ds = DIRECTORY_SEPARATOR;
        $path = dirname(__DIR__) . $ds . '_data' . $ds . 'data.json';
        $this->storedData = json_decode(file_get_contents($path), true);
    }

    private function memberExists($memberId)
    {
        $members = json_encode($this->storedData['members']);
        $output = strpos($members, '5b968faafc13ae46f900004f');

        return $output !== false;
    }

    public function create(Request $request, Response $response, array $args)
    {
        $item = new Item();

        $responseStatus = 409;
        $responseData = [
            'message' => 'Conflict: Player already registered'
        ];

        if ( $item->set($request->getParsedBody()) && $item->save() ) {
            $responseStatus = 200;
            $responseData = $item->get();
        }

        $responseOptions = [
            'request' => $request,
            'response' => $response,
            'methods' => ['POST'],
            'status' => $responseStatus,
            'data' => $responseData,
        ];

        return $this->getResponse($responseOptions);
    }

    public function delete(Request $request, Response $response, array $args) {
        $data = $request->getParsedBody();

        $responseStatus = 500;
        $responseData = [
            'message' => 'Server error...'
        ];

        if ( Item::delete($data['id']) ) {
            $responseStatus = 200;
            $responseData = new \stdClass();
        }

        $responseOptions = [
            'request' => $request,
            'response' => $response,
            'methods' => ['DELETE'],
            'status' => $responseStatus,
            'data' => $responseData,
        ];

        return $this->getResponse($responseOptions);
    }

    public function upload(Request $request, Response $response, array $args)
    {
        echo 'uploading...<br>';

        return $response;
    }

    public function getAll(Request $request, Response $response, array $args)
    {
        $data = $this->storedData['items'];

        $responseStatus = 200;

        $responseOptions = [
            'request' => $request,
            'response' => $response,
            'methods' => ['POST'],
            'status' => $responseStatus,
            'data' => $data,
        ];

        return $this->getResponse($responseOptions);
    }
}