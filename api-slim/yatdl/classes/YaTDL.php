<?php

namespace YaTDL;

use Slim\App;
use YaTDL\Controllers\IndexController;
use YaTDL\Controllers\ItemController;
use YaTDL\Controllers\MemberController;

class YaTDL extends App
{

    public function __construct($container = [])
    {
        parent::__construct($container);
    }

    private function setRoutes()
    {
        $indexController = new IndexController();
        $memberController = new MemberController();
        $itemController = new ItemController();

        $this->get('/', [$indexController, 'index']);

        $routeEntities = [
            'member' => $memberController,
            'list' => $indexController,
            'item' => $itemController,
        ];

        // CRUD Operations
        foreach ( $routeEntities as $entity => $controller ) {
            $this->options('/' . $entity . '/create', [$indexController, 'options']);
            $this->options('/' . $entity . '[/read]', [$indexController, 'options']);
            $this->options('/' . $entity . '/update', [$indexController, 'options']);
            $this->options('/' . $entity . '/delete', [$indexController, 'options']);
            $this->options('/' . $entity . '/all', [$indexController, 'options']);

            $this->post('/' . $entity . '/create', [$controller, 'create']);
            $this->get('/' . $entity . '[/read]', [$controller, 'index']);
            $this->put('/' . $entity . '/update', [$controller, 'update']);
            $this->delete('/' . $entity . '/delete', [$controller, 'delete']);
//            $this->get('/' . $entity . '/delete', [$controller, 'delete']);
            $this->get('/' . $entity . '/all', [$controller, 'getAll']);
        }
    }

    public function start()
    {
        $this->setRoutes();
        $this->run();
    }

}