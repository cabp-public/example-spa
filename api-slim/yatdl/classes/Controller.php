<?php

namespace YaTDL;

class Controller
{

    public $fireData;
    public $analyzer;
    public $dirStructure;
    private $responseOptions;

    public function __construct()
    {
        $this->responseOptions = [
            'request' => false,
            'response' => false,
            'methods' => ['GET'],
            'status' => 200,
            'data' => ['message' => 'Ready'],
        ];
    }

    public function getResponse($options) {
        $data = [];

        foreach ( $this->responseOptions as $key => $defaultValue ) {
            $value = isset($options[$key]) ? $options[$key] : $defaultValue;
            $data[$key] = $value;
        }

        $origins = $data['request']->getHeader('Origin');
        $origin = isset($origins[0]) ? $origins[0] : '*';

        return $data['response']->withStatus($data['status'])
            ->withHeader('Access-Control-Allow-Origin', $origin)
            ->withHeader('Access-Control-Allow-Methods', implode(', ', $data['methods']))
            ->withHeader('Access-Control-Allow-Credentials', 'true')
            ->withHeader('Access-Control-Allow-Headers', 'Content-Type, Access-Control-Allow-Headers, Authorization')
            ->withHeader('Content-Type', 'application/json; charset=utf-8')
            ->write(json_encode($data['data']));
    }

}