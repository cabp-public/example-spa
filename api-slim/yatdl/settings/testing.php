<?php
define('APP_ROOT', dirname(dirname(dirname(__DIR__))));

$settings = [
    'displayErrorDetails' => false,
    'doctrine' => [
        'dev_mode' => false,
        'cache_dir' => APP_ROOT . '/doctrine',
        'connection' => [
            'driver' => 'pdo_mysql',
            'host' => 'localhost',
            'port' => 3306,
            'dbname' => 'yatdl',
            'user' => 'root',
            'password' => 'sndanglg',
            'charset' => 'utf-8'
        ]
    ],
];

return $settings;
