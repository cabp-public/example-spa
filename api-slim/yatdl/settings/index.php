<?php
$environmentsAvail = [
    'yatdl-api-slim.local' => 'development',
    'yatdl-api-slim.test' => 'testing',
    'yatdl-api-slim.prod' => 'production',
];

$envPath = __DIR__ . DIRECTORY_SEPARATOR . $environmentsAvail[$_SERVER['HTTP_HOST']] . '.php';
$settings = include($envPath);

return $settings;
