// Main App
import YATDL from './app';
import * as initOptions from './config/initOptions';

// Styles
import './../node_modules/materialize-css/dist/css/materialize.min.css';
import './assets/css/yatdl.css';

const { constructor: { initEvent } } = YATDL;

String.prototype.replaceAll = function(search, replacement) {
  let target = this;

  return target.split(search).join(replacement);
};

String.prototype.replaceAllRegEx = function(search, replacement) {
  let target = this;
  return target.replace(new RegExp(search, 'gim'), replacement);
};

document.addEventListener(initEvent, function (event) {
  YATDL.render();

  // const { StoreManager } = ReactMarketApp;
  // render(<Provider store={ StoreManager.reduxStore }><MarketApp/></Provider>, document.getElementById('index'));
});

YATDL.init(initOptions.initOptions);
