import Model from '../classes/Model';
import HttpManager from '../classes/static/HttpManager';
import Utilities from '../classes/static/Utilities';

class MemberModel extends Model {
  constructor(main, options) {
    super(main, options);

    this.entity = options.entity;
    this.fields = {
      oid: false,
      name: false,
      email: false
    };
  }

  fetchAll() {
    setTimeout(() => {
      this.onFetchAll(this.fields);
    }, 200);
  }

  /*read(memberId, callback = false) {
    console.log('get new member...');
  }*/

  /*update(data, callback = false) {
    console.log('update member profile...');
  }*/

  /*delete(memberId, callback = false) {
    console.log('delete player');
  }*/

  /*createCallBack(profile) {
    console.log('new member created!');
  }*/

  /*load(profile) {
    this.data = profile;
    localStorage.setItem('yatdl_profile', JSON.stringify(profile));
  }*/

  validate(data) {
    const { name, email } = data;
    const validBool = name !== false && email !== false;
    const validEmail = validBool && email.indexOf('@') > -1 && email.indexOf('.') > -1;

    return validBool && validEmail ? 1 : 0;
  }
}

export default MemberModel;