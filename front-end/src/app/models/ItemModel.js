import Model from '../classes/Model';
import HttpManager from '../classes/static/HttpManager';
import Utilities from '../classes/static/Utilities';

class ItemModel extends Model{
  constructor(main, options) {
    super(main, options);

    this.entity = options.entity;
    this.fields = {
      oid: false,
      title: false,
      description: false,
      image: false
    };
  }

  getAllForCommand(parameters) {
    const { StoreManager } = this.main;
    const { toCarry, callBack } = parameters;
    const storeValue = StoreManager.getKey(this.entity);
    const useStoreVal = Utilities.isValidArray(storeValue) && storeValue.length;
    const fnCallBack = callBack || this.emptyCallBack;
    const httpParams = {
      entity: this.entity,
      operation: 'all',
      callback: fnCallBack
    };

    useStoreVal ? fnCallBack(storeValue, toCarry) : HttpManager.get(httpParams, toCarry);
  }

  /*getAll(callBack) {
    const { entity } = this.options;
    const parameters = {
      entity,
      operation: 'all',
      callback: callBack || this.emptyCallBack
    };

    HttpManager.get(parameters);
  }*/

  /*create(rawData, callBack = false) {
    const { entity } = this.options;
    const isString = typeof rawData === 'string';
    const data = isString ? Utilities.queryStringToJSON(rawData) : rawData;

    const postData = {
      entity,
      operation: this.create.name,
      data,
      callback: callBack || this.emptyCallBack
    };

    console.log(data);
    // this.validate(data) && HttpManager.post(postData);
  }*/

  /*read(memberId, callback = false) {
    console.log('get new member...');
  }*/

  /*update(data, callback = false) {
    console.log('update member profile...');
  }*/

  /*delete(memberId, callback = false) {
    console.log('delete player');
  }*/

  /*emptyCallBack(profile) {
    console.log('new item created!');
  }*/

  /*load(profile) {
    this.data = profile;
    localStorage.setItem('yatdl_profile', JSON.stringify(profile));
  }*/

  validate(data) {
    const { member, title, description } = data;
    return Utilities.isValidString(member) && Utilities.isValidString(title) && Utilities.isValidString(description);
  }
}

export default ItemModel;