// Classes
import StoreManager from './classes/StoreManager';
import LayoutManager from './classes/LayoutManager';
import SessionManager from './classes/SessionManager';
import CommandManager from './classes/CommandManager';
import ViewManager from './classes/ViewManager';

// Models
import MemberModel from './models/MemberModel';
import ItemModel from './models/ItemModel';

// Components
import ErrorModal from './components/ErrorModal/ErrorModal';
import Preloader from './components/Preloader/Preloader';
import SignUp from './components/SignUp/SignUp';
import Navigation from './components/Navigation/Navigation';
import CreateItem from './components/CreateItem/CreateItem';
import GenericList from './components/GenericList/GenericList';
import ItemCounter from './components/ItemCounter/ItemCounter';

class YATDL {
  init(options) {
    const initEventReady = new CustomEvent(YATDL.initEvent);

    // Main Members
    this['models'] = {};
    this['components'] = {
      ErrorModal: new ErrorModal(this, options.components['ErrorModal'] || {})
    };

    // Init ErrorModal
    this.components.ErrorModal.init();

    // Classes
    const classesAvail = {
      StoreManager: new StoreManager(this, options.classes.StoreManager || {}),
      LayoutManager: new LayoutManager(this, options.classes.LayoutManager || {}),
      SessionManager: new SessionManager(this, options.classes.SessionManager || {}),
      CommandManager: new CommandManager(this, options.classes.CommandManager || {}),
      ViewManager: new ViewManager(this, options.classes.ViewManager || {})
    };

    // Models
    const modelsAvail = {
      MemberModel: new MemberModel(this, options.models.MemberModel || {}),
      ItemModel: new ItemModel(this, options.models.ItemModel || {})
    };

    // Components
    const componentsAutoInit = {
      Preloader: new Preloader(this, options.components.Preloader || {}),
      SignUp: new SignUp(this, options.components.SignUp || {}),
      Navigation: new Navigation(this, options.components.Navigation || {}),
      CreateItem: new CreateItem(this, options.components.CreateItem || {}),
      GenericList: new GenericList(this, options.components.GenericList || {}),
      ItemCounter: new ItemCounter(this, options.components.ItemCounter || {})
    };

    Object.keys(classesAvail).forEach((key) => this[key] = classesAvail[key]);
    Object.keys(modelsAvail).forEach((key) => this['models'][key] = modelsAvail[key]);
    Object.keys(componentsAutoInit).forEach((key) => this['components'][key] = componentsAutoInit[key]);

    document.dispatchEvent(initEventReady);
  }

  render() {
    this.LayoutManager.init();
  }
}

YATDL.initEvent = 'INIT_READY';

export default YATDL;