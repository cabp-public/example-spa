import ModalComponent from '../../classes/components/ModalComponent';
import DOMManager from '../../classes/static/DOMManager';

import $ from './../../../../node_modules/cash-dom';
import * as M from 'materialize-css';

import * as appSettings from './../../../config/appSettings';
import template from './CreateItem.html';
import './CreateItem.css';

let ErrorModal;

class CreateItem extends ModalComponent {
  constructor(main, options) {
    super(main, options, template);

    ErrorModal = this.main.components.ErrorModal;

    // Bindings
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onCreateReady = this.onCreateReady.bind(this);
    // this.onFormSubmit = this.onOpenListener.bind(this);
    // this.onCloseListener = this.onCloseListener.bind(this);
    // this.click_close = this.click_close.bind(this);
  }

  init() {
    try {
      const querySelector = appSettings.modals.selectorPrefix + $.camelCase(this.constructor.name);

      this.dom = DOMManager.appendTo({
        target: document.body,
        template: template,
        querySelector
      });

      this.setDataIndex();

      this.dom && this.instantiate();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  instantiate() {
    try {
      const initCallBack = this.eventCallBacks[this.init.name];

      M.Modal.init(this.dom, this.materialOptions);
      this.instance = M.Modal.getInstance(this.dom);

      this.setData();

      this.form = $(this.dom.querySelector('form'))[0];
      this.form.addEventListener('submit', this.onFormSubmit);

      this.openOnInit && this.instance.open();
      this.initialized = true;

      initCallBack && initCallBack();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  onFormSubmit(event) {
    try {
      const { MemberModel, ItemModel } = this.main.models;
      const { Preloader } = this.main.components;
      const serializedData = 'member=' + MemberModel.fields.oid + '&' + $(this.form).serialize();

      serializedData.replaceAll('+', ' ');
      console.log(serializedData);

      Preloader.data = { label: 'creating item' };
      Preloader.open();
      ItemModel.create(serializedData, this.onCreateReady);

      event.preventDefault();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  onCreateReady(item) {
    try {
      const { Preloader, GenericList } = this.main.components;

      // console.log(GenericList);
      GenericList.addItem(item);
      this.close();
      Preloader.close();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  click_close(event) {
    try {
      this.close();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }
}

export default CreateItem;