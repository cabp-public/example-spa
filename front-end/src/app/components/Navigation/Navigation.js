import Component from '../../classes/Component';
import DOMManager from '../../classes/static/DOMManager';
import Utilities from './../../classes/static/Utilities';

import $ from './../../../../node_modules/cash-dom';
import * as M from 'materialize-css';

import * as appSettings from '../../../config/appSettings';
import templateRaw from './Navigation.html';
import './Navigation.css';

let ErrorModal;

class Navigation extends Component {
  constructor(main, options) {
    super(main, options, templateRaw);

    this.click_addActivity = this.click_addActivity.bind(this);

    // Material Options
    this.materialOptions = Utilities.clone(this.options.materialOptions || {});
    this.materialOptions['onOpenEnd'] = this.onOpenListener;
    this.materialOptions['onCloseEnd'] = this.onCloseListener;
  }

  init() {
    try {
      const { wrapUpTag } = appSettings.components;
      const querySelector = appSettings.components.selectorPrefix + $.camelCase(this.constructor.name);
      const template = DOMManager.wrapUpTemplate(templateRaw, wrapUpTag);

      this.dom = DOMManager.appendTo({
        target: document.body,
        template: template,
        querySelector
      });

      this.dom && this.instantiate();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  instantiate() {
    try {
      const initCallBack = this.eventCallBacks[this.init.name];
      const element = document.querySelector(this.options.querySelector);

      DOMManager.killDefaultAnchors($(element));

      this.instance = M.Sidenav.init(element, this.materialOptions);
      this.initialized = true;

      initCallBack && initCallBack();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  open() {
    this.instance.open();
  }

  close() {
    this.instance.close();
  }

  onOpenListener() {
  }

  onCloseListener() {
  }

  click_addActivity(event) {
    try {
      const { CreateItem } = this.main.components;

      CreateItem.open();
      this.close();

      event.preventDefault();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  click_openSettings(event) {
    try {
      console.log('openSettings!');

      event.preventDefault();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }
}

export default Navigation;