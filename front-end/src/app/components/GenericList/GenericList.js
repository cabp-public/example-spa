import Component from '../../classes/Component';
import DOMManager from '../../classes/static/DOMManager';
import ListItem from './../ListItem/ListItem';
import Utilities from '../../classes/static/Utilities';

import $ from './../../../../node_modules/cash-dom';
import * as M from 'materialize-css';

import * as appSettings from '../../../config/appSettings';
import templateRaw from './GenericList.html';
import './GenericList.css';

let ErrorModal;

class GenericList extends Component {
  constructor(main, options) {
    super(main, options, templateRaw);

    this.totalItems = 0;

    this.addItem = this.addItem.bind(this);
  }

  get total() {
    const { StoreManager } = this.main;
    const allItems = StoreManager.getKey('item');

    this.totalItems = allItems.length;

    return this.totalItems;
  }

  set total(value) {
    const { StoreManager } = this.main;
    const allItems = StoreManager.getKey('item');
    this.totalItems = allItems.length;
  }

  init() {
    try {
      const { selectorPrefix, wrapUpTag } = appSettings.components;
      const querySelector = selectorPrefix + $.camelCase(this.constructor.name);
      const className = querySelector.substring(1, querySelector.length);
      const template = DOMManager.wrapUpTemplate(templateRaw, wrapUpTag, className);

      this.dom = DOMManager.appendTo({
        target: document.body,
        template: template,
        querySelector: querySelector
      });

      this.dom && this.instantiate();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  instantiate() {
    try {
      const initCallBack = this.eventCallBacks[this.init.name];
      initCallBack && initCallBack();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  addItem(model) {
    try {
      const { ViewManager } = this.main;
      const { ItemCounter } = this.main.components;
      const componentClass = ViewManager.getController('ListItem');
      const dummyComp = new componentClass(this.main, -1, {});
      const template = Utilities.cloneString(dummyComp.template);

      ViewManager.buildAndAppend({
        componentClass,
        template,
        model,
        target: this.dom.firstChild
      });

      this.totalItems++;

      ItemCounter.data = {
        total: this.totalItems
      };
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }
}

export default GenericList;