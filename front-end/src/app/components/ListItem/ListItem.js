import Component from './../../classes/Component';
import DOMManager from '../../classes/static/DOMManager';

import $ from './../../../../node_modules/cash-dom';
import * as M from 'materialize-css'

import * as appSettings from '../../../config/appSettings';
import template from './ListItem.html';
import './ListItem.css';

class ListItem extends Component {
  constructor(main, options) {
    super(main, options, template);

    this.id = false;
    this.click_deleteItem = this.click_deleteItem.bind(this);
    this.onDeleteItem = this.onDeleteItem.bind(this);
  }

  set newDom(value) {
    this.dom = value;
  }

  set oid(value) {
    this.id = value;
    /*this.modelAttribute = {
      name: 'id',
      value
    };*/
  }

  init() {
    const { wrapUpTag, selectorPrefix } = appSettings.components;
    const selectorClass = selectorPrefix.substring(1, selectorPrefix.length) + this.constructor.name;

    this.dom = DOMManager.wrapUpTemplate(template, wrapUpTag, selectorClass);
    this.dom && this.instantiate();
  }

  instantiate() {
    const initCallBack = this.eventCallBacks[this.init.name];
    // DOMManager.killDefaultAnchors($(this.dom));
    initCallBack && initCallBack();
  }

  click_deleteItem(event) {
    try {
      const { ItemModel } = this.main.models;
      const { Preloader } = this.main.components;

      Preloader.data = { label: 'deleting note' };
      Preloader.open();
      ItemModel.delete(this.id, this.onDeleteItem);
    }
    catch(e) {
      console.error(e);
      // ErrorModal.open(e);
    }

    event.preventDefault();
  }

  onDeleteItem(data) {
    const { Preloader, GenericList, ItemCounter } = this.main.components;

    ItemCounter.data = {
      total: GenericList.total
    };

    $(this.dom).remove();
    Preloader.data = { label: '' };
    Preloader.close();
  }
}

export default ListItem;