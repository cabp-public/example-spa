import ModalComponent from '../../classes/components/ModalComponent';
import DOMManager from '../../classes/static/DOMManager';

import $ from './../../../../node_modules/cash-dom';
import * as M from 'materialize-css';

import * as appSettings from './../../../config/appSettings';
import template from './ErrorModal.html';
import './ErrorModal.css';

class ErrorModal extends ModalComponent {
  constructor(main, options) {
    super(main, options, template);

    // Default data sample
    /*
    this.nodeData = {
      name: false,
      message: false
    };
    */
  }

  set label(value) {
  }

  init() {
    try {
      const querySelector = appSettings.modals.selectorPrefix + $.camelCase(this.constructor.name);

      this.dom = DOMManager.appendTo({
        target: document.body,
        template: template,
        querySelector
      });

      this.setDataIndex();

      this.dom && this.instantiate();
    }
    catch(e) {
      console.error(e);
    }
  }

  instantiate() {
    try {
      const initCallBack = this.eventCallBacks[this.init.name];

      M.Modal.init(this.dom, this.materialOptions);
      this.instance = M.Modal.getInstance(this.dom);

      this.setData();
      this.openOnInit && this.instance.open();
      this.initialized = true;

      initCallBack && initCallBack();
    }
    catch(e) {
      console.error(e);
    }
  }

  open(e) {
    appSettings.devMode && console.error(e);

    this.data = {
      name: e.constructor.name,
      message: e.message
    };

    this.instance.open();
  }
}

export default ErrorModal;
