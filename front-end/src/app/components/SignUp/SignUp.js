import ModalComponent from '../../classes/components/ModalComponent';
import DOMManager from '../../classes/static/DOMManager';

import $ from './../../../../node_modules/cash-dom';
import * as M from 'materialize-css';

import * as appSettings from './../../../config/appSettings';
import template from './SignUp.html';
import './SignUp.css';

let ErrorModal;

class SignUp extends ModalComponent {
  constructor(main, options) {
    super(main, options, template);

    ErrorModal = this.main.components.ErrorModal;

    // Bindings
    this.onFormSubmit = this.onFormSubmit.bind(this);
    this.onSignUpReady = this.onSignUpReady.bind(this);
  }

  init() {
    try {
      const querySelector = appSettings.modals.selectorPrefix + $.camelCase(this.constructor.name);

      this.dom = DOMManager.appendTo({
        target: document.body,
        template: template,
        querySelector
      });

      this.setDataIndex();

      this.dom && this.instantiate();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  instantiate() {
    try {
      const initCallBack = this.eventCallBacks[this.init.name];

      M.Modal.init(this.dom, this.materialOptions);
      this.instance = M.Modal.getInstance(this.dom);

      this.setData();

      this.form = $(this.dom.querySelector('form'))[0];
      this.form.addEventListener('submit', this.onFormSubmit);

      this.openOnInit && this.instance.open();
      this.initialized = true;

      initCallBack && initCallBack();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  onFormSubmit(event) {
    try {
      const { MemberModel } = this.main.models;
      const { Preloader } = this.main.components;

      Preloader.data = { label: 'creating profile' };
      Preloader.open();
      MemberModel.create($(this.form).serialize(), this.onSignUpReady);

      event.preventDefault();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  onSignUpReady(profile) {
    try {
      const { LayoutManager, SessionManager } = this.main;

      if ( SessionManager.start(profile) ) {
        LayoutManager.loadModelsData();
        this.close();
      }
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }
}

export default SignUp;