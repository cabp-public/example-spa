import Component from '../../classes/Component';
import DOMManager from '../../classes/static/DOMManager';
import ListItem from './../ListItem/ListItem';
import Utilities from '../../classes/static/Utilities';

import $ from './../../../../node_modules/cash-dom';
import * as M from 'materialize-css';

import * as appSettings from '../../../config/appSettings';
import template from './ItemCounter.html';
import './ItemCounter.css';

let ErrorModal;

class ItemCounter extends Component {
  constructor(main, options) {
    super(main, options, template);

    this.nodeData = {
      total: 0
    };
  }

  /*set data(value) {
    this.nodeData.total = parseInt(value, 10);

    console.log(this.nodeData);
    this.setDataIndex();

    setTimeout(() => {
      this.setData();
    }, 1000);
  }*/

  init() {
    try {
      this.dom = DOMManager.appendTo({
        target: document.body,
        template: template,
        querySelector: this.options.querySelector
      });

      this.dom && this.instantiate();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  instantiate() {
    try {
      const initCallBack = this.eventCallBacks[this.init.name];
      initCallBack && initCallBack();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }
}

export default ItemCounter;