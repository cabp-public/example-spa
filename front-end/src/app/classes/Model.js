import MasterClass from '../classes/MasterClass';
import HttpManager from '../classes/static/HttpManager';
import Utilities from '../classes/static/Utilities';

class Model extends MasterClass {
  constructor(main, options, entity) {
    super(main, options);

    this.entity = entity;
    this.fields = {};
    this.eModelFetchAll = false;

    // Bindings
    this.onFetchAll = this.onFetchAll.bind(this);
  }

  set data(value) {
    this.fields = value;
  }

  set eventFetchAll(value) {
    this.eModelFetchAll = value;
  }

  create(rawData, callback = false) {
    const data = Utilities.isValidString(rawData) ? Utilities.queryStringToJSON(rawData) : rawData;

    const postData = {
      entity: this.entity,
      operation: this.create.name,
      callback: callback || this.emptyCallBack,
      data
    };

    this.validate(data) && HttpManager.post(postData);
  }

  read(memberId, callback = false) {
    // console.log('read ' + this.options.entity);
  }

  update(data, callback = false) {
    // console.log('update ' + this.options.entity);
  }

  delete(id, callback = false) {
    const { StoreManager } = this.main;
    const storedData = StoreManager.getKey(this.entity);
    const newStoreData = [];

    // Remove from store
    storedData.forEach((item) => {
      id !== item['$oid'] && newStoreData.push(item);
    });

    // Remove from DB
    HttpManager.delete({
      entity: this.entity,
      operation: 'delete',
      callback: callback || this.emptyCallBack,
      data: { oid: id }
    });

    /*const data = Utilities.isValidString(rawData) ? Utilities.queryStringToJSON(rawData) : rawData;

    console.log('delete ' + id + ' from ' + this.options.entity);

    const postData = {
      entity: this.options.entity,
      operation: this.create.name,
      callback: callback || this.emptyCallBack,
      data
    };

    this.validate(data) && HttpManager.post(postData);*/
  }

  fetchAll() {
    const parameters = {
      entity: this.entity,
      operation: 'all',
      callback: this.onFetchAll || this.emptyCallBack
    };

    HttpManager.get(parameters);
  }

  onFetchAll(data) {
    const { StoreManager } = this.main;

    StoreManager.setKey({
      type: this.entity,
      value: data
    });

    setTimeout(() => {
      document.dispatchEvent(this.eModelFetchAll);
    }, 200);
  }

  emptyCallBack() {
    // console.log('operation over ' + this.options.entity + ' completed (default callback)');
  }
}

export default Model;