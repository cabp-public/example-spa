import Utilities from './static/Utilities';
import EventManager from './static/EventManager';

import * as appSettings from './../../config/appSettings';
import $ from './../../../node_modules/cash-dom';

const { components, commandAttribute } = appSettings;
const { dataSetDelay } = components;
const dataSetEventQueue = [];

let nodeCount = 0;
let dataSetCount = 0;

class DataComponent {
  constructor(main, options, template, viewsAvail = []) {
    this.main = main;
    this.options = options;
    this.template = template;
    this.dom = false;
    this.viewsAvail = viewsAvail;
    this.extDataReadyEvent = false;

    // Node Data & Event Handlers
    this.nodeDataExpect = 0;

    this.nodeDataQueue = {
      data: [],
      command: [],
      listeners: []
    };
    this.nodeDataSet = [];

    // Events
    this.eventCallBacks = {};

    // Bindings
    this.setNext = this.setNext.bind(this);
    this.setEventListeners = this.setEventListeners.bind(this);
  }

  get dataRequired() {
    const { dataRequired } = this.options;
    const isValidObject = Utilities.isValidObject(dataRequired);

    return isValidObject ? dataRequired : true;
  }

  set setDataReadyEvent(value) {
    this.extDataReadyEvent = value;
  }

  set initCallBack(callBack) {
    this.eventCallBacks['init'] = callBack || false;
  }

  setNext() {
    if ( dataSetCount === this.nodeDataSet.length - 1 ) {
      this.onDataAndHandlersReady();
    }
    else {
      dataSetCount++;
      this.setDataAndHandlers();
    }
  }

  // TODO: the curlyBracesRegex constant is not working within this method
  setDataFields(node) {
    const events = dataSetEventQueue[dataSetCount];
    const readyEvent = events[this.setDataFields.name];
    const html = $(node).html();

    const curlyMatches = html.match(/{{(.*)}}/gim);
    // const curlyMatches = html.match(curlyBracesRegex);
    let model, fieldValue;

    if ( Utilities.isValidString(html) && Utilities.isValidArray(curlyMatches) ) {
      curlyMatches.forEach((match) => {
        const dataFields = Utilities.extractDataField(match);
        const { modelName, field } = dataFields;

        model = this.main.models[modelName] || false;
        fieldValue = model ? model.data[field] : false;

        fieldValue && $(node).html(html.replace(match, fieldValue));
      });
    }

    setTimeout(() => {
      document.dispatchEvent(readyEvent);
    }, dataSetDelay);
  }

  // TODO: eventAllReady is not in use
  setDataAndHandlers() {
    const eventNameSetDataFields = this.setDataFields.name + '_Ready_' + dataSetCount;
    const eventSetDataFields = EventManager.executeAfterEvent(eventNameSetDataFields, this.setNext);

    const eventNameAllReady = 'eventAll_Ready_' + dataSetCount;
    const eventAllReady = EventManager.executeAfterEvent(eventNameAllReady, this.onDataAndHandlersReady);

    const node = this.nodeDataSet[dataSetCount];

    dataSetEventQueue[dataSetCount] = {
      setDataFields: eventSetDataFields,
      eventAllReady,
    };

    this.setDataFields(node);
  }

  nodeNeedsData(node) {
    const html = $(node).html();
    const curlyMatches = html.match(/{{(.*)}}/gim);

    return (Utilities.isValidString(html) && Utilities.isValidArray(curlyMatches)) && curlyMatches.length === 1;
  };

  checkDataNeeds() {
    this.nodeDataQueue.data.forEach((node) => {
      if ( this.nodeNeedsData(node) ) {
        this.nodeDataSet.push(node);
      }
    });

    if ( this.nodeDataSet.length ) {
      this.setDataAndHandlers();
    }

    if ( this.nodeDataQueue.command.length ) {
      this.onDataAndHandlersReady();
    }
  }

  // TODO: abstract this if madness...!
  setNodeDataQueue(node) {
    const children = $(node).children();
    const hasCommand = Utilities.isValidObject($(node).attr(commandAttribute));

    if ( children.length && !hasCommand ) {
      for ( let i = 0; i < children.length; i++ ) {
        this.setNodeDataQueue(children[i]);
      }
    }
    else {
      this.nodeDataQueue.data.push(node);
    }

    if ( hasCommand ) {
      this.nodeDataQueue.command.push(node);
    }

    if ( nodeCount === this.nodeDataExpect ) {
      this.checkDataNeeds();
    }

    nodeCount++;
  }

  setData() {
    try {
      const allTags = this.template.match(/<\/?[\w\s="/.':;#-\/?]+>/gim);
      const cleanTags = [];

      allTags.forEach((tag) => {
        const close = tag.indexOf('</') === 0;
        const command = tag.indexOf(commandAttribute) > -1;

        (!close && !command) && cleanTags.push(tag);
      });

      this.nodeDataExpect = cleanTags.length;
      nodeCount = 0;

      this.setNodeDataQueue(this.dom);

      return true;
    }
    catch(error) {
      console.error(error);
      return false;
    }
  }

  setEventListeners(node) {
    const children = $(node).children();
    const attributes = node.attributes;

    $(attributes).each((i) => {
      const attr = attributes[i];
      const name = attr.name.replace('data-', '');
      const methodName = name + '_' + $(attr).val();
      const method = this[methodName] || false;

      if ( method ) {
        $(node).on(name, method);
      }
    });

    if ( children.length ) {
      for ( let i = 0; i < children.length; i++ ) {
        this.setEventListeners(children[i]);
      }
    }
  }

  // TODO: remove that setTimeout, use events instead
  setListeners(event) {
    try {
      this.setEventListeners(this.dom);

      setTimeout(() => {
        document.dispatchEvent(event);
      }, dataSetDelay);
    }
    catch(error) {
      console.error(error);
      return false;
    }
  }

  onDataAndHandlersReady() {
    const { CommandManager } = this.main;

    CommandManager.domQueue = this.nodeDataQueue.command;
    CommandManager.setDataReadyEvent = this.extDataReadyEvent;
    CommandManager.setCommands(this.dom);
  }
}

export default DataComponent;