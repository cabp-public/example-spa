import Component from './../Component';
import Utilities from '../static/Utilities';

class ModalComponent extends Component {
  constructor(main, options, template) {
    super(main, options, template);

    this.instance = {};
    this.state = { active: false };

    // Open after instantiation
    this.openOnInit = options['openOnInit'];

    // Bindings
    this.onOpenListener = this.onOpenListener.bind(this);
    this.onCloseListener = this.onCloseListener.bind(this);

    // Material Options
    this.materialOptions = Utilities.clone(this.options.modal);
    this.materialOptions['onOpenEnd'] = this.onOpenListener;
    this.materialOptions['onCloseEnd'] = this.onCloseListener;
  }

  set initCallBack(callBack) {
    this.eventCallBacks['init'] = callBack || false;
  }

  set openEnd(callBack) {
    this.eventCallBacks['onOpenEnd'] = callBack || false;
  }

  set closeEnd(callBack) {
    this.eventCallBacks['onCloseEnd'] = callBack || false;
  }

  open() {
    this.instance.open();
  }

  close() {
    this.instance.close();
  }

  onOpenListener() {
    const { onOpenEnd } = this.eventCallBacks|| false;
    const executeCallBack = onOpenEnd !== false ? onOpenEnd : false;

    this.state.active = true;
    executeCallBack && onOpenEnd();
  }

  onCloseListener() {
    const { onCloseEnd } = this.eventCallBacks || false;
    const executeCallBack = onCloseEnd !== false ? onCloseEnd : false;

    this.state.active = false;
    executeCallBack && onCloseEnd();
  }
}

export default ModalComponent;