import Utilities from './static/Utilities';
import EventManager from './static/EventManager';

import * as appSettings from './../../config/appSettings';
import $ from './../../../node_modules/cash-dom';

const { dataSeparator } = appSettings.template;
let nodeCount = 0;

class Component {
  constructor(main, options, template) {
    this.main = main;
    this.options = options;
    this.template = template;
    this.dom = false;
    this.eventCallBacks = {};
    this.nodeDataIndex = [];
    this.attrDataIndex = [];
    this.nodeData = {};
    this.initialized = false;

    this.eSetCommands = false;
    this.eSetListeners = false;

    // Bindings
    this.setDataIndex = this.setDataIndex.bind(this);
    this.setListeners = this.setListeners.bind(this);
    this.setNodeListeners = this.setNodeListeners.bind(this);
    this.onSetCommands = this.onSetCommands.bind(this);
  }

  get dataRequired() {
    const { dataRequired } = this.options;
    const isValidObject = Utilities.isValidObject(dataRequired);

    return isValidObject ? dataRequired : true;
  }

  set initCallBack(callBack) {
    this.eventCallBacks['init'] = callBack || false;
  }

  set data(value) {
    const nodes = this.dom.querySelectorAll('*');
    const template = $(Utilities.clone(this.template))[0];
    const templateNodes = template.querySelectorAll('*');

    nodes.forEach(
      function(node, index, listObj) {
        const children = $(node).children();
        const innerText = node.innerText.replaceAll(/\r?\n|\r/, '').trim();

        if ( children.length === 0 && innerText.length ) {
          node.innerText = templateNodes[index].innerText;
        }
      }
    );

    this.nodeData = value;
    this.nodeDataIndex = [];

    this.setDataIndex();
    this.setData();
  }

  set eventSetCommands(value) {
    try {
      this.eSetCommands = value;
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  set eventSetListeners(value) {
    try {
      this.eSetListeners = value;
    }
    catch(e) {
      console.error(e);
      // ErrorModal.open(e);
    }
  }

  set modelAttribute(value) {
    $(this.dom).attr(appSettings.dataAttrPrefixModel + value.name, value.value);
  }

  setDataIndex() {
    try {
      const regEx = /{{([^}}]+)?}}/gim;
      const nodeDataIndex = [];
      let match, model, fieldValue;

      while ( match = regEx.exec(this.template) ) {
        const expression = match[1].trim();
        const allData = expression.split(dataSeparator);
        const key = String(allData[0]).trim();
        const tplData = allData[1] || false;
        const tplValue = tplData ? tplData.substring(1, tplData.length - 2) : false;
        const defaultValue = Object.keys(this.nodeData).indexOf(key) >= 0 ? this.nodeData[key] : tplValue;
        const value = Utilities.isValidObject(defaultValue) ? defaultValue : tplValue;
        const dataField = Utilities.extractDataField(expression);

        if ( !Utilities.isValidBoolean(dataField) ) {
          model = this.main.models[dataField.modelName] || false;
          fieldValue = model ? model.fields[dataField.field] : false;
        }

        const dataItem = {
          key,
          expression: match[0],
          value: !Utilities.isUndefined(fieldValue) ? fieldValue : value
        };

        nodeDataIndex.push(dataItem);
      }

      nodeDataIndex.forEach((item) => {
        const canPush = JSON.stringify(this.nodeDataIndex).indexOf(item.expression) === -1;
        canPush && this.nodeDataIndex.push(item);
      });
    }
    catch(e) {
      console.error(e);
      // ErrorModal.open(e);
    }
  }

  setNodeData(node) {
    let innerText = node.innerText;

    this.nodeDataIndex.forEach((item) => {
      if ( !Utilities.isUndefined(item.value) ) {
        innerText = innerText.replaceAll(item.expression, item.value);
      }
    });

    node.innerText = innerText;
  }

  setData() {
    const self = this;
    const nodes = this.dom.querySelectorAll('*');

    nodes.forEach(
      function(node, index, listObj) {
        const children = $(node).children();
        const lastElement = children.length === 0 && node.innerText.length;

        self.setNodeAttrDataIndex(node);
        lastElement && self.setNodeData(node);
      }
    );
  }

  setCommands() {
    try {
      const { CommandManager } = this.main;
      const nodes = this.dom.querySelectorAll('*');
      const eventName = appSettings.defaultEvents.setCommands + '_' + this.constructor.name; // eSetCommands_[Navigation]
      const domCommandQueue = [];

      // this.eSetCommands
      CommandManager.eventCommandSet = EventManager.createEvent(eventName, this.onSetCommands);

      nodes.forEach(
        function(node, index, listObj) {
          const hasCommand = Utilities.isValidObject($(node).attr(appSettings.commandAttribute));

          hasCommand && domCommandQueue.push(node);
        }
      );

      CommandManager.domQueue = domCommandQueue;
      CommandManager.setOnQueue();
    }
    catch(e) {
      console.error(e);
      // ErrorModal.open(e);
    }
  }

  onSetCommands() {
    document.dispatchEvent(this.eSetCommands);
  }

  setNodeListeners(node, index, listObj) {
    const attributes = node.attributes;

    $(attributes).each((i) => {
      const attr = attributes[i];
      const name = attr.name.replace('data-', '');
      const methodName = name + '_' + $(attr).val();
      const method = this[methodName] || false;

      method && $(node).on(name, method);
    });

    if ( nodeCount === listObj.length - 1 ) {
      this.onSetListeners();
    }

    nodeCount++;
  }

  setListeners() {
    const nodes = this.dom.querySelectorAll('*');

    nodeCount = 0;
    nodes.forEach(this.setNodeListeners);
  }

  onSetListeners() {
    this.eSetListeners && document.dispatchEvent(this.eSetListeners);
  }

  // TODO: implement this method
  setNodeAttrData(node) {
  }

  // TODO: implement this method
  setNodeAttrDataIndex(node) {
  }

  // TODO: implement this method
  reload() {
  }
}

export default Component;