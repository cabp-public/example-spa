import Utilities from '../static/Utilities';
import $ from './../../../../node_modules/cash-dom';

class RepeatCommand {
  constructor(main) {
    this.main = main;
    this.eCommandExecuted = false;
    // this.readyQueue = [];

    this.execute = this.execute.bind(this);
  }

  set eventCommandExecuted(value) {
    this.eCommandExecuted = value;
  }

  execute(target, componentClass, data) {
    const { ViewManager } = this.main;

    try {
      const dummyComp = new componentClass(this.main, -1, {});
      const templateRaw = Utilities.cloneString(dummyComp.template);
      let template = templateRaw;

      data.forEach((model) => {
        ViewManager.buildAndAppend({
          componentClass,
          template,
          model,
          target
        });

        template = templateRaw;
      });

      setTimeout(() => {
        document.dispatchEvent(this.eCommandExecuted);
      }, 200);
    }
    catch(error) {
      console.error(error);
      return false;
    }
  }
}

export default RepeatCommand;