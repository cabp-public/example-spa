import MasterClass from './MasterClass';
import Utilities from './static/Utilities';
import EventManager from './static/EventManager';
import ViewsAvail from './../components';
import ListItem from './../components/ListItem/ListItem';

import * as appSettings from './../../config/appSettings';
import $ from './../../../node_modules/cash-dom';

const controllersAvail = { ListItem };
let ErrorModal;

class ViewManager extends MasterClass {
  constructor(main, options) {
    super(main, options);
  }

  getController(viewName, options = {}) {
    const keys = Object.keys(ViewsAvail);
    const viewAvail = keys.indexOf(viewName) >= 0;
    const classRef = (viewAvail && controllersAvail[viewName]) || false;

    return viewAvail && controllersAvail[viewName];
    // return classRef ? new classRef(this.main, options) : false;
  }

  buildAndAppend(parameters) {
    const regEx = /{{([^}}]+)?}}/gim;
    let { componentClass, template, model, target } = parameters;
    let component, match;

    component = new componentClass(this.main, {});
    component.init();

    while ( match = regEx.exec(template) ) {
      const expression = match[1];
      const dataField = Utilities.extractDataField(expression);
      const { field } = dataField;
      const value = model[field];

      template = template.replace(match[0], value);
    }

    component.newDom = $(template)[0];
    component.oid = model['$oid'];

    $(target).append(component.dom);

    component.setData();
    component.setListeners();
  }
}

export default ViewManager;