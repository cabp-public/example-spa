import Utilities from './static/Utilities';
import EventManager from './static/EventManager';
import * as appSettings from './../../config/appSettings';
import $ from './../../../node_modules/cash-dom';

const { components, commandAttribute } = appSettings;
const { dataSetDelay } = components;
const dataSetEventQueue = [];

let nodeCount = 0;
let dataSetCount = 0;

class EmptyComponent {
  constructor(options, template) {
    this.dom = false;
    this.extDom = false;
    this.template = false;
  }
}

export default EmptyComponent;