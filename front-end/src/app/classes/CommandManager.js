import MasterClass from './MasterClass';
import Utilities from './static/Utilities';
import EventManager from './static/EventManager';
import ViewsAvail from './../components';
import RepeatCommand from './Commands/RepeatCommand';

import * as appSettings from './../../config/appSettings';
import $ from './../../../node_modules/cash-dom';

const { commandsAvail, commandFnPrefix, commandAttribute } = appSettings;
let ErrorModal;
let nodeCount = 0;
let externalDom;

class CommandManager extends MasterClass {
  constructor(main, options) {
    super(main, options);

    ErrorModal = this.main.components.ErrorModal;

    this.domCommandQueue = false;
    this.settingCommands = false;
    this.eCommandSet = false;

    // Bindings
    this.commandExecute = this.commandExecute.bind(this);
    this.onCommandExecute = this.onCommandExecute.bind(this);
  }

  set eventCommandSet(value) {
    this.eCommandSet = value;
  }

  set domQueue(value) {
    this.domCommandQueue = value;
  }

  /*set dataReadyCallBack(value) {
    this.dataSetCallBack = value;
  }*/

  /*set setDataReadyEvent(value) {
    this.extDataReadyEvent = value;
  }*/

  commandAvail(name) {
    const availOnSettings = commandsAvail.indexOf(name) >= 0;
    const methodName = commandFnPrefix + Utilities.stringCapitalizeFirst(name);
    const availToExecute = this[methodName] || false;

    return availOnSettings && availToExecute;
  }

  // TODO: remove event listeners automatically
  commandExecute(data, parameters) {
    const { command, componentClass, parentNode } = parameters;
    const eventName = appSettings.defaultEvents.commandExecuted + '_' + command.constructor.name;

    command.eventCommandExecuted = EventManager.createEvent(eventName, this.onCommandExecute);
    command.execute(parentNode, componentClass, data);
  }

  onCommandExecute(event) {
    if ( this.settingCommands ) {
      if ( nodeCount === this.domCommandQueue.length - 1 ) {
        this.onSetOnQueue();
      }
      else {
        nodeCount++;
        this.setCommand();
      }
    }
    else {
      // console.log('  command executed...!');
    }
  }

  commandRepeat(parameters) {
    const { ViewManager } = this.main;
    const { node, modelName, viewName } = parameters;
    const componentClass = ViewManager.getController(viewName);
    const command = new RepeatCommand(this.main);
    const parentNode = node.parentNode;
    const model = this.main.models[modelName + 'Model'] || false;
    const params = {
      toCarry: { command, componentClass, parentNode },
      callBack: this.commandExecute
    };

    if ( componentClass ) {
      node.parentNode.removeChild(node);
      model && model.getAllForCommand(params);
    }
  }

  setCommand() {
    try {
      const node = this.domCommandQueue[nodeCount];
      const expression = $(node).attr(commandAttribute);
      let statement = Utilities.extractCommandStatement(expression);

      statement.params.node = node;

      this.commandAvail(statement.name) && this[statement.method](statement.params);
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  setOnQueue() {
    try {
      nodeCount = 0;
      this.settingCommands = true;

      if ( this.domCommandQueue.length ) {
        this.setCommand();
      }
      else {
        document.dispatchEvent(this.eCommandSet);
      }
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  onSetOnQueue() {
    this.settingCommands = false;
    document.dispatchEvent(this.eCommandSet);
  }
}

export default CommandManager;