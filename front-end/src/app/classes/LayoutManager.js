import MasterClass from './MasterClass';
import Utilities from './static/Utilities';
import EventManager from './static/EventManager';

import * as appSettings from './../../config/appSettings';
import $ from './../../../node_modules/cash-dom';

let ErrorModal;
let initComponentIndex = 0;
let componentCount = 0;
let modelCount = 0;

class LayoutManager extends MasterClass {
  constructor(main, options) {
    super(main, options);

    ErrorModal = this.main.components.ErrorModal;

    this.componentsKeys = {};
    this.modelsKeys = {};
    this.toCommandQueue = [];

    // Bindings
    this.initComponent = this.initComponent.bind(this);
    this.onComponentInit = this.onComponentInit.bind(this);
    this.onComponentsInit = this.onComponentsInit.bind(this);
    this.onLoadModelData = this.onLoadModelData.bind(this);
    this.onInitComponentCommands = this.onInitComponentCommands.bind(this);
    this.onInitComponentListeners = this.onInitComponentListeners.bind(this);
  }

  init() {
    try {
      this.componentsKeys = Object.keys(this.main.components);
      this.initComponent();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  initComponent() {
    try {
      const key = this.componentsKeys[initComponentIndex] || false;
      const component = this.main.components[key] || false;
      const canInit = !component.initialized;

      if (canInit) {
        component.initCallBack = this.onComponentInit;
        component.init();
      }
      else {
        this.onComponentInit();
      }
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  loadModelData() {
    try {
      const key = this.modelsKeys[modelCount];
      const model = this.main.models[key];
      const eventNameNext = appSettings.defaultEvents.modelFetchAll + '_' + modelCount;

      model.eventFetchAll = EventManager.createEvent(eventNameNext, this.onLoadModelData);
      model.fetchAll();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  loadModelsData() {
    try {
      modelCount = 0;
      this.modelsKeys = Object.keys(this.main.models);
      this.loadModelData();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  // TODO: set a major event for initializing commands after updating data
  updateComponentsData() {
    try {
      const keys = Object.keys(this.main.components);
      let component;

      keys.forEach((key) => {
        component = this.main.components[key];
        component.setDataIndex();
        component.setData();
      });

      this.initCommands();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  onComponentInit() {
    try {
      if (initComponentIndex === this.componentsKeys.length - 1) {
        this.onComponentsInit();
      }
      else {
        initComponentIndex++;
        this.initComponent();
      }
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  onComponentsInit() {
    try {
      const { SessionManager } = this.main;

      // TODO: abstract this, 250ms is the default 'inDuration' value for the material modal
      setTimeout(() => {
        SessionManager.init();
      }, 250);
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  // TODO: event listeners should be removed automatically
  onLoadModelData(event) {
    try {
      const eventName = appSettings.defaultEvents.modelFetchAll + '_' + modelCount;
      document.removeEventListener(eventName, event);

      if ( modelCount === this.modelsKeys.length - 1 ) {
        this.updateComponentsData();
      }
      else {
        modelCount++;
        this.loadModelData();
      }
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  initCommands() {
    try {
      const keys = Object.keys(this.main.components);
      let component;

      componentCount = 0;
      this.toCommandQueue = [];

      keys.forEach((key) => {
        component = this.main.components[key];
        component.dataRequired && this.toCommandQueue.push(component);
      });

      this.initComponentCommands();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  initComponentCommands() {
    try {
      const component = this.toCommandQueue[componentCount];
      const eventNameNext = appSettings.defaultEvents.setCommands + '_' + componentCount;

      component.eventSetCommands = EventManager.createEvent(eventNameNext, this.onInitComponentCommands);
      component.setCommands();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  // TODO: event listeners should be removed automatically
  onInitComponentCommands(event) {
    try {
      const component = this.toCommandQueue[componentCount];
      const eventName = {
        count: appSettings.defaultEvents.setCommands + '_' + componentCount,
        component: appSettings.defaultEvents.setCommands + '_' + component.constructor.name
      };

      document.removeEventListener(eventName.component, component.onSetCommands);
      document.removeEventListener(eventName.count, this.onInitComponentCommands);

      if ( componentCount === this.toCommandQueue.length - 1 ) {
        this.initListeners();
      }
      else {
        componentCount++;
        this.initComponentCommands();
      }
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  initListeners() {
    try {
      componentCount = 0;
      this.initComponentListeners();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  initComponentListeners() {
    const componentKey = this.componentsKeys[componentCount];
    const component = this.main.components[componentKey];
    const eventNameNext = appSettings.defaultEvents.setListeners + '_' + component.constructor.name;

    component.eventSetListeners = EventManager.createEvent(eventNameNext, this.onInitComponentListeners);
    component.setListeners();
  }

  onInitComponentListeners(event) {
    if ( componentCount === this.componentsKeys.length - 1 ) {
      this.onLayoutReady();
    }
    else {
      componentCount++;
      this.initComponentListeners();
    }
  }

  onLayoutReady() {
    const { Preloader, GenericList, ItemCounter } = this.main.components;

    ItemCounter.data = {
      total: GenericList.total
    };

    Preloader.data = { label: 'Loading' };
    Preloader.close();
  }
}

export default LayoutManager;