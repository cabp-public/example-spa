import MasterClass from './MasterClass';
import { createStore, combineReducers } from 'redux';
// import { ComponentListeners } from '../config/ComponentListeners';
import * as initState from './../../config/initState';

class StoreManager extends MasterClass {
  constructor(main, options) {
    super(main, options);

    const config = initState.initState;
    const reducers = {};

    Object.keys(config).forEach(function (key) {
      reducers[key] = function (currentState = config[key], action) {
        return (action.type === key) ? action.value : currentState;
      };
    });

    this.reduxStore = createStore(combineReducers(reducers), {});

    // Bindings
    this.getKey = this.getKey.bind(this);
    this.setKey = this.setKey.bind(this);
    this.setKeys = this.setKeys.bind(this);
    this.mapStateToProps = this.mapStateToProps.bind(this);
  }

  /**
   * Get Single Key
   */
  getKey(key) {
    const state = this.reduxStore.getState();
    return state[key] || false;
  }

  /**
   * Set or Update Single Key
   */
  setKey(action) {
    this.reduxStore.dispatch(action);
  }

  /**
   * Set several keys from the given array of actions
   * @param params
   */
  setKeys(params) {
    const self = this;

    params.forEach(function (action) {
      self.setKey(action);
    })
  }

  /**
   * Returns the key-value pairs from the state to a component
   * @param componentName
   * @param state
   * @returns {{}}
   */
  mapStateToProps(componentName, state) {
    // const keys = ComponentListeners[componentName];
    const values = {};

    /*keys.forEach(function (key) {
      values[key] = state[key];
    });*/

    return values;
  }
}

export default StoreManager;