import Utilities from './static/Utilities';

import * as appSettings from './../../config/appSettings';

class SessionManager {
  constructor(main, options) {
    this.main = main;
    this.options = options;

    this.state = {
      started: false
    };
  }

  static get sessionStarted() {
    return this.state.started;
  }

  init() {
    try {
      const { LayoutManager } = this.main;
      const { prefix, profile } = appSettings.localStorage.prefix;
      const localKey = prefix + '_' + profile;
      const localDataRaw = localStorage.getItem(localKey) || '';
      const localData = Utilities.isValidString(localDataRaw) ? JSON.parse(localDataRaw) : false;

      if ( localData && this.start(localData) ) {
        LayoutManager.loadModelsData();
      }
      else {
        this.triggerRegister();
      }
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  triggerRegister() {
    try {
      const { Preloader, SignUp } = this.main.components;

      Preloader.close();
      SignUp.open();
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  // TODO: implement proper LogIn process
  triggerLogin(profile) {
    try {}
    catch(e) {
      ErrorModal.open(e);
    }
  }

  start(profile) {
    try {
      const { MemberModel } = this.main.models;
      const { prefix } = appSettings.localStorage.prefix;
      const localKey = prefix + '_' + appSettings.localStorage.prefix.profile;

      localStorage.setItem(localKey, JSON.stringify(profile));
      MemberModel.data = profile;

      return true;
    }
    catch(e) {
      ErrorModal.open(e);
    }
  }

  end() {
  }
}

export default SessionManager;