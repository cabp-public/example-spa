import Utilities from './Utilities';

class EventManager {
  static executeAfterEvent(eventName, callBack, callBackParams = null) {
    const event = new CustomEvent(eventName);

    document.addEventListener(eventName, (e) => {
      callBackParams !== null && callBack(callBackParams, e);

      setTimeout(() => {
        document.removeEventListener(eventName, EventManager.executeAfterEvent);
      }, 200);
    });

    return event;
  }

  static createEvent(eventName, listener) {
    const event = new CustomEvent(eventName);
    document.addEventListener(eventName, listener);

    return event;
  }
}

export default EventManager;