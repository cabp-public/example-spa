import Utilities from './Utilities';
import $ from '../../../../node_modules/cash-dom/dist/cash.esm';

class DOMManager {
  static wrapUpTemplate(value, tag, className = false) {
    let template = '';

    template += '<' + tag + (className ? ' class="' + className + '"' : '') + '>';
    template += value + '</' + tag + '>';

    return template;
  }

  static appendTo(parameters) {
    const { target, template, elementId, querySelector } = parameters;
    let element;

    try {
      $(template).appendTo(target);

      if ( Utilities.isValidObject(elementId) ) {
        element = document.getElementById(elementId);
      }
      else {
        element = Utilities.isValidString(querySelector) ? document.querySelector(querySelector) : false;
      }

      return element !== false ? element : true;
    }
    catch(error) {
      console.error(error);
      return false;
    }
  }

  // TODO: setting even listeners shouldn't require a loop...
  static killDefaultAnchors(collection = false) {
    const anchors = collection || $('a');
    let anchor;

    anchors.each((i) => {
      anchor = $(anchors[i])[0];

      anchor.click((event) => {
        event.preventDefault();
      });
    });
  }

  static killDefaults() {
    DOMManager.killDefaultAnchors();
  }
}

export default DOMManager;
