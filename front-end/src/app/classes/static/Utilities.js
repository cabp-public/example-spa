import * as appSettings from './../../../config/appSettings';

class Utilities {
  static cloneString(value) {
    const json = Utilities.cloneObject(value);
    let parsed = '';

    Object.keys(json).forEach((key) => {
      parsed += json[key];
    });

    return parsed;
  }

  static cloneObject(value) {
    const keys = Object.keys(value);
    const parsed = {};

    keys.forEach((key) => {
      parsed[key] = value[key];
    });

    return parsed;
  }

  static clone(value) {
    const type = typeof value;
    let parsed;

    switch (type) {
      case 'string':
        parsed = Utilities.cloneString(value);
        break;

      default:
        parsed = Utilities.cloneObject(value);
    }

    return parsed;
  }

  static extractDataField(value) {
    const modelField = value.trim().split('.');
    const modelName = modelField[0] + 'Model';
    const field = modelField[1] || false;

    return field ? { modelName, field } : false;
  }

  static extractCommandStatement(value) {
    const parts = value.split('(');
    const name = parts[0].trim();
    const options = parts[1].replace(')', '').trim().split(':');
    const method = appSettings.commandFnPrefix + Utilities.stringCapitalizeFirst(name);

    return {
      name,
      method,
      params: {
        modelName: options[0].trim(),
        viewName: options[1].trim(),
        node: false
      }
    };
  }

  static stringCapitalizeFirst(value) {
    const capital = String(value[0]).toUpperCase();
    const rest = value.substr(1, value.length);

    return capital + rest;
  }

  static isNull(value) {
    return value === null;
  }

  static isUndefined(value, strictType = true) {
    return value === undefined;
  }

  static isValidObject(value, strictType = true) {
    return !Utilities.isNull(value) && !Utilities.isUndefined(value);
  }

  static isValidString(value, strict = true) {
    const isValidObject = Utilities.isValidObject(value);
    const isString = isValidObject && (typeof value === 'string');
    const hasLength = isString ? value.length : false;

    return strict ? hasLength : isString;
  }

  static isValidArray(value) {
    return Utilities.isValidObject(value) && Array.isArray(value);
  }

  static isValidBoolean(value) {
    return (typeof value) === 'boolean';
  }

  static queryStringToJSON(value) {
    const decodedValue = decodeURIComponent(value);

    let output = false;

    try {
      const pairs = decodedValue.split('&');
      output = {};

      pairs.forEach((pair) => {
        const keyValue = pair.split('=');
        output[keyValue[0]] = String(keyValue[1]).replaceAll('+', ' ');
      });

      return output;
    }
    catch (error) {
      return output;
    }
  }
}

export default Utilities;