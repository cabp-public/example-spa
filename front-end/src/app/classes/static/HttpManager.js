import * as appSettings from '../../../config/appSettings';

const { api } = appSettings;

class HttpManager {
  static get(parameters, toCarry = false) {
    const { entity, operation, callback } = parameters;
    const url = api.protocol + '://' + api.domain + '/' + entity + '/' + operation;

    // console.log('executing a GET request');

    fetch(url)
      .then((response) => {
        if ( !response.ok ) {
          throw Error(response.statusText);
        }

        return response.json();
      })
      .then((json) => {
        callback !== false && callback(json, toCarry);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  static post(parameters) {
    const { entity, operation, callback, data } = parameters;
    const url = api.protocol + '://' + api.domain + '/' + entity + '/' + operation;

    // console.log('executing a POST request');

    const params = {
      method: 'POST',
      headers: api.headers,
      mode: 'cors',
      body: JSON.stringify(data)
    };

    fetch(url, params)
      .then((response) => {
        if ( !response.ok ) {
          throw Error(response.statusText);
        }

        return response.json();
      })
      .then((json) => {
        callback !== false && callback(json);
      })
      .catch((error) => {
        console.error(error);
      });
  }

  static put() {
    // console.log('execute a PUT request');
  }

  static delete(parameters) {
    const { entity, operation, callback, data } = parameters;
    const url = api.protocol + '://' + api.domain + '/' + entity + '/' + operation;

    console.log('executing a DELETE request');

    const params = {
      method: 'DELETE',
      headers: api.headers,
      mode: 'cors',
      body: JSON.stringify(data)
    };

    console.log(url);
    console.log(params);

    fetch(url, params)
      .then((response) => {
        if ( !response.ok ) {
          throw Error(response.statusText);
        }

        return response;
      })
      .then((json) => {
        // console.log(json);
        callback !== false && callback(json);
      })
      .catch((error) => {
        console.error(error);
      });
  }
}

export default HttpManager;